﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dao.User;
using AHCBL.Dto;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.User
{
    public class MainController : Controller
    {
        Permission checkuser = new Permission();
        // GET: main
        public ActionResult Index()
        {
            checkuser.chkrights("user");
            var data = MainDao.Instance.GetDataList();
            decimal amt = GetDataDao.Instance.GetPointAmount(Varible.User.member_id);
            decimal amt_interest = GetDataDao.Instance.GetPointInterest(Varible.User.member_id);
            //decimal amt_withdrawal = GetDataDao.Instance.GetPointWithdrawal(Varible.User.member_id);

            ViewBag.point = Util.NVLInt(amt);
            ViewBag.amount = Util.NVLInt(amt) <1000 ? Util.NVLInt(amt).ToString() : amt.ToString("0,0", CultureInfo.InvariantCulture);
            ViewBag.interest = Util.NVLInt(amt_interest) < 1000 ? Util.NVLInt(amt_interest).ToString() : amt_interest.ToString("0,0", CultureInfo.InvariantCulture);
            ViewBag.withdrawal = Util.NVLInt(amt_interest) < 1000 ? Util.NVLInt(amt_interest).ToString() : amt_interest.ToString("0,0", CultureInfo.InvariantCulture);//Util.NVLInt(amt_withdrawal) < 1000 ? Util.NVLInt(amt_withdrawal).ToString() : amt_withdrawal.ToString("0,0", CultureInfo.InvariantCulture);
            ViewBag.persen = Util.NVLInt(Varible.Config.point_percent1);
            return View(data);
        }
        public ActionResult Savedata(string fee, string amt, string total, string interest)
        {
            try
            {
                int id = 0;
                string result = MainDao.Instance.SaveDataList(id, fee, amt, total, interest,"add");

                if (result == "OK")
                {
                    return Json(new returnsave { err = "0", errmsg = "성공하셨습니다 !" });
                }
                else
                {
                    return Json(new returnsave { err = "1", errmsg = "에러." });
                }
            }
            catch 
            {
                return Json(new returnsave { err = "1", errmsg = "에러." });
            }

        }
    }
}