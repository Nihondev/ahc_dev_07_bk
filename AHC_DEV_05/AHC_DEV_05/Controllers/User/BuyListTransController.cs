﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_DEV_05.Controllers.User
{
    public class BuyListTransController : Controller
    {
        Permission checkuser = new Permission();
        // GET: BuyListTrans
        public ActionResult Index(int? page)
        {
            checkuser.chkrights("user");
            var data = RequestListDao.Instance.GetSummaryList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            //var data1 = RequestListDao.Instance.GetDataList().Find(smodel => smodel.id == id);
            var count = data.Count;
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
           
            return View(TempData["data"]);
        }

        public ActionResult Sell_history(int? page)
        {
            checkuser.chkrights("user");
            var data = RequestListDao.Instance.GetSellList();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            return View(TempData["data"]);
        }
    }
}