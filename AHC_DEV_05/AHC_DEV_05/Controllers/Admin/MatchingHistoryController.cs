﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using OfficeOpenXml;
using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_DEV_05.Controllers.Admin
{
    public class MatchingHistoryController : Controller
    {
        Permission checkuser = new Permission();
        // GET: MathchingHistory
        public ActionResult Index(int? page, string drp, string keyword)
        {
            checkuser.chkrights("admin");
            var data = MatchingListDao.Instance.GetDataList(); 
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;

            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            if (keyword != null)
            {
                if (drp == "username")
                {
                    TempData["data"] = data.Where(x => x.username_buy == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.username_buy == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.username_buy == keyword || keyword == null || keyword == "").ToList().Count;
                }
                if (drp == "date")
                {
                    TempData["data"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().Count;
                }
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;
            Session["data"] = TempData["data1"];

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);
        }
        public ActionResult ExportToExcel()
        {
            try
            {
                var data = Session["data"] as List<ProductOrderDto>;
                if (data != null)
                {
                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    ExcelPackage Ep = new ExcelPackage();
                    ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                    Sheet.Cells["A1"].Value = "NO.";
                    Sheet.Cells["B1"].Value = "상품번호";
                    Sheet.Cells["C1"].Value = "구매자ID";
                    Sheet.Cells["D1"].Value = "구매자";
                    Sheet.Cells["E1"].Value = "판매자ID";
                    Sheet.Cells["F1"].Value = "판매자";
                    Sheet.Cells["G1"].Value = "상품";
                    Sheet.Cells["H1"].Value = "가격";
                    Sheet.Cells["I1"].Value = "수수료";
                    Sheet.Cells["J1"].Value = "생성일";
                    Sheet.Cells["K1"].Value = "마지막 거래일";
                    Sheet.Cells["L1"].Value = "상태";
                    int row = 2;
                    foreach (var item in data)
                    {
                        Sheet.Cells[string.Format("A{0}", row)].Value = item.num.ToString();
                        Sheet.Cells[string.Format("B{0}", row)].Value = item.code;
                        Sheet.Cells[string.Format("C{0}", row)].Value = item.username_buy;
                        Sheet.Cells[string.Format("D{0}", row)].Value = item.fullname_buy;
                        Sheet.Cells[string.Format("E{0}", row)].Value = item.username_sale;
                        Sheet.Cells[string.Format("F{0}", row)].Value = item.fullname_sale;
                        Sheet.Cells[string.Format("G{0}", row)].Value = item.product_name;
                        Sheet.Cells[string.Format("H{0}", row)].Value = item.price_interest;
                        Sheet.Cells[string.Format("I{0}", row)].Value = item.fee;
                        Sheet.Cells[string.Format("J{0}", row)].Value = item.create_date;
                        Sheet.Cells[string.Format("K{0}", row)].Value = item.buy_date;
                        Sheet.Cells[string.Format("L{0}", row)].Value = item.status;
                        row++;
                    }
                    Sheet.Cells["A:AZ"].AutoFitColumns();
                    Response.Clear();
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                    Response.BinaryWrite(Ep.GetAsByteArray());
                    Response.End();


                }
                ViewBag.Search = Session["Search"];
                ViewBag.Count = Session["Count"];
                ViewBag.Rows = Session["Rows"];

                return View("Index");
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }
    }
}