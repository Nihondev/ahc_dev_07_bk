﻿using AHCBL.Component.Common;
using AHCBL.Dao;
using AHCBL.Dao.Admin;
using AHCBL.Dto.Admin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AHC_MLK.Controllers.Admin
{
    public class MatchingListController : Controller
    {
        // GET: MatchingList
        public ActionResult Index(int? page, string drp, string keyword)
        {
            var data = MatchingListDao.Instance.GetData();
            int rows = Util.NVLInt(Varible.Config.page_rows);
            var count = data.Count;
            ViewBag.Status = DropdownDao.Instance.GetStatusTran();
            TempData["data"] = data.ToList().ToPagedList(page ?? 1, rows);
            TempData["data1"] = data.ToList();
            if (keyword != null)
            {
                if (drp == "username")
                {
                    TempData["data"] = data.Where(x => x.username_buy == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.username_buy == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.username_buy == keyword || keyword == null || keyword == "").ToList().Count;
                }
                if (drp == "date")
                {
                    TempData["data"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().ToPagedList(page ?? 1, Util.NVLInt(rows));
                    TempData["data1"] = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList();
                    count = data.Where(x => x.create_date.Substring(0, 10) == keyword || keyword == null || keyword == "").ToList().Count;
                }
            }
            Session["Search"] = DropdownDao.Instance.GetDrpSearch();
            Session["Count"] = count;
            Session["Rows"] = rows;
            Session["data"] = TempData["data1"];

            ViewBag.Search = Session["Search"];
            ViewBag.Count = Session["Count"];
            ViewBag.Rows = Session["Rows"];

            return View(TempData["data"]);


        }
        public ActionResult Updatedata(string status,int id)
        {
            try
            {

                ProductOrderDto model = new ProductOrderDto();
                model.id = id;
                if (Convert.ToInt32(status) == 0)
                {
                    string result = MatchingListDao.Instance.SaveMatchingStatus(model);
                    if (result != "OK")
                    {
                        return Json(result);
                    }
                    else
                    {
                        return Json("Successfully !");
                    }
                }
                else
                {
                    return Json("Not Update Data status is not fail !");
                }
            }
            catch (Exception e)
            {
                return Json("Error !!");
            }
        }
    }
}