﻿using AHCBL.Component.Common;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static AHCBL.Dto.GetDataDto;

namespace AHCBL.Dao
{
    public class GetDataDao : BaseDao<GetDataDao>    
    {
        private DataTable dt;
        public int GetPointAmount(int id)
        {
            int tatal = 0;
            dt = GetStoredProc("PD056_GET_POINT_MEMBER", new string[] { "@memberid" }, new string[] { Util.NVLString(id) });
            foreach (DataRow row in dt.Rows)
            {
                tatal = Util.NVLInt(row["total"]);
            }
            return tatal;
        }

        public int GetPointInterest(int id)
        {
            int tatal = 0;
            dt = GetStoredProc("PD097_GET_POINT_INTEREST", new string[] { "@memberid" }, new string[] { Util.NVLString(id) });
            foreach (DataRow row in dt.Rows)
            {
                tatal = Util.NVLInt(row["total"]);
            }
            return tatal;
        }
        public int GetPointWithdrawal(int id)
        {
            int tatal = 0;
            dt = GetStoredProc("PD098_GET_POINT_WITHDRAWAL", new string[] { "@memberid" }, new string[] { Util.NVLString(id) });
            foreach (DataRow row in dt.Rows)
            {
                tatal = Util.NVLInt(row["total"]);
            }
            return tatal;
        }




























        public int GetRequestSumAmount()
        {
            int tatal = 0;
            dt = GetStoredProc("PD033_GET_REQUERT_COUNT_AMOUNT");
            foreach (DataRow row in dt.Rows)
            {
                tatal = Util.NVLInt(row["total"]);
            }
            return tatal;
        }
        public int GetWithdrawalSumAmount(string user_name)
        {
            int tatal = 0;
            dt = GetStoredProc("PD034_GET_WITHDRAWAL_COUNT_AMOUNT", new string[] { "@user_name" }, new string[] { Util.NVLString(user_name) });
            foreach (DataRow row in dt.Rows)
            {
                tatal = Util.NVLInt(row["total"]);
            }
            return tatal;
        }
        public int GetPointAmount()
        {
            int tatal = 0;
            dt = GetStoredProc("PD047_GET_POINT_COUNT_AMOUNT");
            foreach (DataRow row in dt.Rows)
            {
                tatal = Util.NVLInt(row["total"]);
            }
            return tatal;
        }
       


        public ProductMember GetProductMember()
        {
            ProductMember data = new ProductMember();
            dt = GetStoredProc("PD075_GET_PRODUCT_MEMBER", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });

            foreach (DataRow row in dt.Rows)
            {
                data.buy_id = Util.NVLInt(3);
                data.buy = Util.NVLInt(row["buy"]);
                data.sale_id = Util.NVLInt(4);
                data.sale = Util.NVLInt(row["sale"]);
            }

            return data;
        }
        public MemberListDto GetAdmin(int member_id)
        {
            MemberListDto list = new MemberListDto();
            dt = GetStoredProc("PD068_GET_DATA_ADMIN", new string[] { "@member_id" }, new string[] { Util.NVLString(member_id) });

            foreach (DataRow row in dt.Rows)
            {
                list.acc_name = Util.NVLString(row["acc_name"]);
                list.acc_no = Util.NVLString(row["acc_no"]);
                list.txt1 = Util.NVLString(row["bank_name"]);
            }


            //list.acc_name = dt[0]["accname"];


            //foreach (DataRow row in dt.Rows)
            //{
            //    tatal = Util.NVLInt(row["total"]);
            //}
            return list;
        }
        public List<LogDto> GetLogLogin(int member_id)
        {
            List<LogDto> list = new List<LogDto>();
            dt = GetStoredProc("PD039_GET_LOG", new string[] { "@memberid" }, new string[] { Util.NVLString(member_id) });
            foreach (DataRow dr in dt.Rows)
            {
                list.Add(
                    new LogDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        ip = Util.NVLString(dr["ip"]),
                        url = Util.NVLString(dr["url"]),
                        browser = Util.NVLString(dr["browser"]),
                        system_os = Util.NVLString(dr["system_os"]),
                        connect_hardware = Util.NVLString(dr["connect_hardware"]),
                        start_date = Util.NVLString(dr["start_date"]),
                        stop_date = Util.NVLString(dr["stop_date"]),
                        member_id = Util.NVLInt(dr["member_id"])
                    });

            }
           
            return list;
        }

        [NonAction]
        public SelectList GetYearLog()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            dt = GetStoredProc("PD070_GET_YEAR_LOG");
            list.Add(new SelectListItem { Text = "년도선택", Value = "" });
            foreach (DataRow dr in dt.Rows)
            {
                list.Add(new SelectListItem { Text = Util.NVLString(dr["y"]), Value = Util.NVLString(dr["y"]) });

            }
            return new SelectList(list, "Value", "Text");
        }

        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        public int GenItemNumber()
        {
            int itemNo = 0;
            dt = GetStoredProc("PD046_GEN_ITEM_NUMBER");
            foreach (DataRow dr in dt.Rows)
            {
                itemNo = Util.NVLInt(dr["itemNo"]);

            }
            return itemNo;
        }
        public int ChkDay()
        {
            int itemNo = 0;
            dt = GetStoredProc("PD065_GET_DAY");
            foreach (DataRow dr in dt.Rows)
            {
                itemNo = Util.NVLInt(dr["itemNo"]);

            }
            return itemNo;
        }
    }
}
