﻿using AHCBL.Component.Common;
using AHCBL.Dto;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class VisitListDao : BaseDao<VisitListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<LogDto> GetDataList(string start_date, string stop_date)
        {
            try
            {
                List<LogDto> list = new List<LogDto>();
                dt = GetStoredProc("PD069_GET_LOGIN", new string[] { "@startdate", "@stopdate" }, new string[] { Util.NVLString(start_date), Util.NVLString(stop_date) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new LogDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        ip = Util.NVLString(dr["ip"]),
                        url = Util.NVLString(dr["url"]),
                        browser = Util.NVLString(dr["browser"]),
                        system_os = Util.NVLString(dr["system_os"]),
                        connect_hardware = Util.NVLString(dr["connect_hardware"]),
                        start_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        stop_date = dr["stop_date"].ToString()=="" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["stop_date"]).ToString("yyyyMMdd"))),

                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<LogDto> GetDataList()
        {
            try
            {
                List<LogDto> list = new List<LogDto>();
                dt = GetStoredProc("PD021_GET_LOG_LOGIN");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new LogDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        ip = Util.NVLString(dr["ip"]),
                        url = Util.NVLString(dr["url"]),
                        browser = Util.NVLString(dr["browser"]),
                        system_os = Util.NVLString(dr["system_os"]),
                        connect_hardware = Util.NVLString(dr["connect_hardware"]),
                        start_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        st_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMdd"))),
                        stop_date = dr["stop_date"].ToString() == "" ? "" : Util.NVLString(Cv.Date(Convert.ToDateTime(dr["stop_date"]).ToString("yyyyMMdd"))),

                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DelLogin(string year, string month, string method)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD071_DEL_LOGIN", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_year", Util.NVLString(year));
                AddSQLParam(param, "@p_month", Util.NVLString(month));
                AddSQLParam(param, "@p_method", Util.NVLString(method));
                conn.Open();
                //cmd.ExecuteNonQuery();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
        //public string SaveDataList(VisitListDto model, string action)
        //{
        //    string result = "OK";
        //    try
        //    {
        //        conn = CreateConnection();
        //        MySqlCommand cmd = new MySqlCommand("PD022_SAVE_LOG_VISIT", conn);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        MySqlParameterCollection param = cmd.Parameters;
        //        param.Clear();
        //        AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
        //        AddSQLParam(param, "@ip", Util.NVLString(model.ip));
        //        AddSQLParam(param, "@name", Util.NVLString(model.name));
        //        AddSQLParam(param, "@browser", Util.NVLString(model.browser));
        //        AddSQLParam(param, "@os", Util.NVLString(model.os));
        //        AddSQLParam(param, "@drive", Util.NVLString(model.drive));
        //        AddSQLParam(param, "@status", action);

        //        conn.Open();
        //        MySqlDataReader read = cmd.ExecuteReader();
        //        while (read.Read())
        //        {
        //            result = read.GetString(0).ToString();
        //        }
        //        conn.Close();
        //    }
        //    catch (Exception e)
        //    {
        //        result = e.Message.ToString();
        //    }
        //    return result;
        //}
    }
}