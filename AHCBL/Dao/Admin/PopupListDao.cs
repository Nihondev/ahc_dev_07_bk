﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.Admin
{
    public class PopupListDao : BaseDao<PopupListDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<PopupListDto> GetDataList()
        {
            try
            {
                List<PopupListDto> list = new List<PopupListDto>();
                dt = GetStoredProc("PD023_GET_POPUP");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new PopupListDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        name = Util.NVLString(dr["name"]),
                        grp_id = Util.NVLInt(dr["grp_id"]),
                        drive_id = Util.NVLInt(dr["drive_id"]),
                        drive_name = Util.NVLString(dr["drive_name"]),
                        disable_hours = Util.NVLString(dr["disable_hours"]),
                        start_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["start_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        stop_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["stop_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        pop_left = Util.NVLString(dr["pop_left"]),
                        pop_top = Util.NVLString(dr["pop_top"]),
                        pop_width = Util.NVLString(dr["pop_width"]),
                        pop_height = Util.NVLString(dr["pop_height"]),
                        detail = Util.NVLString(dr["detail"]),


                    });

                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string SaveDataList(PopupListDto model, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD024_SAVE_POPUP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
                AddSQLParam(param, "@name", Util.NVLString(model.name));
                AddSQLParam(param, "@grp_id", Util.NVLString(model.grp_id));
                AddSQLParam(param, "@drive_id", Util.NVLString(model.drive_id));
                AddSQLParam(param, "@disable_hours", Util.NVLString(model.disable_hours));
                AddSQLParam(param, "@start_date", Util.NVLString(model.start_date));
                AddSQLParam(param, "@stop_date", Util.NVLString(model.stop_date+" 23:59:59"));
                AddSQLParam(param, "@pop_left", Util.NVLString(model.pop_left));
                AddSQLParam(param, "@pop_top", Util.NVLString(model.pop_top));
                AddSQLParam(param, "@pop_width", Util.NVLString(model.pop_width));
                AddSQLParam(param, "@pop_height", Util.NVLString(model.pop_height));
                AddSQLParam(param, "@detail", Util.NVLString(model.detail));
                AddSQLParam(param, "@member_id", Util.NVLInt(1));
                AddSQLParam(param, "@status", action);

                conn.Open();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}