﻿using AHCBL.Component.Common;
using AHCBL.Dto.Admin;
using AHCBL.Dto.User;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AHCBL.Dto.User.OrderDto;

namespace AHCBL.Dao.User
{
    public class OrderDao : BaseDao<OrderDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<Order2> GetInterest()
        {
            try
            {
                string wait = ConfigurationManager.AppSettings["wait"];
                string approve = ConfigurationManager.AppSettings["approve"];
                List<Order2> list = new List<Order2>();
                dt = GetStoredProc("PD051_GET_INTEREST", new string[] { "@user_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    decimal point = Util.NVLDecimal(dr["interest"].ToString().Replace("-", ""));
                    decimal price = Util.NVLDecimal(dr["price"].ToString().Replace("-", ""));
                    decimal total = (price - point);
                    list.Add(new Order2
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        name = Util.NVLString(total < 1000 ? total.ToString() : total.ToString("0,0", CultureInfo.InvariantCulture)),
                        price = Util.NVLString(price < 1000 ? price.ToString() : price.ToString("0,0", CultureInfo.InvariantCulture)),
                        win_price = Util.NVLString(point < 1000 ? point.ToString() : point.ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        action = Util.NVLString(Util.NVLInt(dr["status"]) == 2 ? wait : approve),
                    });
                }
                return list;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Order2> GetWithdraw()
        {
            try
            {
                List<Order2> list = new List<Order2>();
                dt = GetStoredProc("PD047_GET_WITHDRAWL", new string[] { "@user_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    decimal point = Util.NVLDecimal(dr["price"].ToString().Replace("-", ""));
                    list.Add(new Order2
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        name = Util.NVLString((Util.NVLDecimal(dr["interest"])- point)),
                        sale_date = Util.NVLString(Util.NVLDecimal(dr["interest"]) < 1000 ? dr["interest"].ToString() : Util.NVLDecimal(dr["interest"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        price = Util.NVLString(point < 1000 ? point.ToString() : point.ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });
                }
                return list;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Order2> GetAdd()
        {
            try
            {
                string wait = ConfigurationManager.AppSettings["wait"];
                string approve = ConfigurationManager.AppSettings["approve"];
                string cancal = ConfigurationManager.AppSettings["cancal"];
                
                List<Order2> list = new List<Order2>();
                dt = GetStoredProc("PD043_GET_BUY_PRODUCT", new string[] { "@user_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(new Order2
                    {
                        id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        action = Util.NVLString(Util.NVLInt(dr["status"]) == 1 ? wait : Util.NVLInt(dr["status"]) == 0 ? cancal : approve),
                        //sale_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["sale_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        price = Util.NVLString(Util.NVLDecimal(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLDecimal(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                    });
                }
                return list;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<Order2> GetBuy()
        {
            try
            {
                List<Order2> data = new List<Order2>();
                dt = GetStoredProc("PD106_GET_PRODUCT_ORDER_LIST", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    data.Add(new Order2
                    {
                        id = Util.NVLInt(dr["id"]),
                        hp = Util.NVLString(dr["num"]),
                        price = Util.NVLString(dr["amount"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                        sale_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["sale_date"]).ToString("yyyyMMdd"))),
                        ////img = Util.NVLString(dr["img"]),
                        //name = Util.NVLString(dr["name"]),
                        //buy = Util.NVLInt(dr["buy"]),
                        ////buy_appv = Util.NVLInt(dr["buy_appv"]),
                        //appv = Util.NVLInt(dr["appv"]),
                        ////sale = Util.NVLInt(dr["sale"]),
                        //complete = Util.NVLInt(dr["complete"]),

                    });
                }
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<Order2> GetSale()
        {
            try
            {
                List<Order2> data = new List<Order2>();
                dt = GetStoredProc("PD107_GET_PRODUCT_ORDER_LIST", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    data.Add(new Order2
                    {
                        //id = Util.NVLInt(dr["id"]),
                        num = Util.NVLInt(dr["num"]),
                        price = Util.NVLString(dr["price_interest"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                        //sale_date = Util.NVLString(dr["price_win"]),
                        sale_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["sale_date"]).ToString("yyyyMMdd"))),
                        name = Util.NVLString(dr["code"]),
                        ////img = Util.NVLString(dr["img"]),
                        //name = Util.NVLString(dr["name"]),
                        //buy = Util.NVLInt(dr["buy"]),
                        ////buy_appv = Util.NVLInt(dr["buy_appv"]),
                        //appv = Util.NVLInt(dr["appv"]),
                        ////sale = Util.NVLInt(dr["sale"]),
                        //complete = Util.NVLInt(dr["complete"]),

                    });
                }
                return data;
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        public List<Order2> GetBenefit()
        {
            try
            {
                List<Order2> data = new List<Order2>();
                dt = GetStoredProc("PD108_GET_POINT_INTEREST", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    data.Add(new Order2
                    {
                        num = Util.NVLInt(dr["num"]),
                        price = Util.NVLString(dr["interest"]),
                        remark = Util.NVLString(dr["remark"]),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["sale_date"]).ToString("yyyyMMdd"))),
                        //active = Util.NVLString(Util.NVLInt(dr["pay_type"]) == 7 ? "원금" : "이자"),
                    });
                }
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public OrderDto GetProductOrder(int id, string status)
        {
            try
            {
                OrderDto data = new OrderDto();
                dt = GetStoredProc("PD076_GET_PRODUCT_ORDER", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab1.Add(new Order1
                    {
                        id = Util.NVLInt(dr["id"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        img = Util.NVLString(dr["img"]),
                        name = Util.NVLString(dr["name"]),
                        buy = Util.NVLInt(dr["buy"]),
                        //buy_appv = Util.NVLInt(dr["buy_appv"]),
                        appv = Util.NVLInt(dr["appv"]),
                        //sale = Util.NVLInt(dr["sale"]),
                        complete = Util.NVLInt(dr["complete"]),

                    });
                }

                dt = GetStoredProc("PD062_GET_PRODUCT_ORDER_LIST", new string[] { "@member_id", "@product_id", "@action" }, new string[] { Util.NVLString(Varible.User.member_id), Util.NVLString(id), Util.NVLString(status) });
                foreach (DataRow dr in dt.Rows)
                {
                    string  aaa = Util.NVLString(dr["amount"]).Replace("-", "");
                    data.tab2.Add(new Order2
                    {
                        id = Util.NVLInt(dr["id"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        //img = Util.NVLString(dr["img"]),
                        name = Util.NVLString(dr["name"]),
                        fullname = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["expdate"]).ToString("yyyyMMddHH:mm:ss"))),
                        price = Util.NVLString(Util.NVLDecimal(aaa) < 1000 ? aaa.ToString() : Util.NVLDecimal(aaa).ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMddHH:mm:ss"))),
                        status = Util.NVLInt(dr["status"]),
                    });
                }

                return data;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public OrderDto GetProductSaleOrder(int id, string status, string action)
        {
            try
            {
                OrderDto data = new OrderDto();
                dt = GetStoredProc("PD082_GET_PRODUCT_SALE_ORDER", new string[] { "@member_id", "@action" }, new string[] { Util.NVLString(Varible.User.member_id), Util.NVLString(action) });
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab1.Add(new Order1
                    {
                        id = Util.NVLInt(dr["id"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        img = Util.NVLString(dr["img"]),
                        name = Util.NVLString(dr["name"]),
                        buy = Util.NVLInt(dr["buy"]),
                        //buy_appv = Util.NVLInt(dr["buy_appv"]),
                        appv = Util.NVLInt(dr["appv"]),
                        //sale = Util.NVLInt(dr["sale"]),
                        complete = Util.NVLInt(dr["complete"]),

                    });
                }

                dt = GetStoredProc("PD062_GET_PRODUCT_ORDER_LIST", new string[] { "@member_id", "@product_id", "@action" }, new string[] { Util.NVLString(Varible.User.member_id), Util.NVLString(id), Util.NVLString(status) });
                foreach (DataRow dr in dt.Rows)
                {
                    data.tab2.Add(new Order2
                    {
                        id = Util.NVLInt(dr["id"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        img = Util.NVLString(dr["img"]),
                        name = Util.NVLString(dr["name"]),
                        //price = Util.NVLString(dr["price"]),
                        price = Util.NVLString(Util.NVLInt(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLInt(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                        status = Util.NVLInt(dr["status"]),
                    });
                }

                return data;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public OrderDto GetProductAppv(int product_id)
        {
            try
            {
                OrderDto data = new OrderDto();
                dt = GetStoredProc("PD067_GET_PRODUCT_APPV", new string[] { "@member_id", "@product_id" }, new string[] { Util.NVLString(Varible.User.member_id), Util.NVLString(product_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    //int win = (((Util.NVLInt(dr["price"]) / 100) * Util.NVLInt(dr["percen"]))+ Util.NVLInt(dr["price"]));
                    data.tab2.Add(new Order2
                    {
                        id = Util.NVLInt(dr["id"]),
                        token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                        img = Util.NVLString(dr["img"].ToString()),
                        name = Util.NVLString(dr["name"].ToString()),
                        fullname = Util.NVLString(dr["fullname"]),
                        hp = Util.NVLString(dr["code"]),
                        price = Util.NVLString(Util.NVLInt(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLInt(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        buy_info = Util.NVLString(dr["buy_info"]),
                        //percen = Util.NVLInt(dr["percen"]),
                        win_price = Util.NVLString(Util.NVLInt(dr["price_win"]) < 1000 ? dr["price_win"].ToString() : Util.NVLInt(dr["price_win"]).ToString("0,0", CultureInfo.InvariantCulture)),
                    });

                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public OrderDto GetProductSale(int product_id)
        {
            try
            {
                OrderDto data = new OrderDto();
                dt = GetStoredProc("PD078_GET_PRODUCT_SALE", new string[] { "@memberid", "@productid" }, new string[] { Util.NVLString(Varible.User.member_id), Util.NVLString(product_id) });
                if (dt.Rows.Count != 0)
                {
                    if (dt.Rows[0]["id"].ToString() != "")
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            data.tab2.Add(new Order2
                            {
                                id = Util.NVLInt(dr["id"]),
                                token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                                hp = Util.NVLString(dr["code"]),
                                img = Util.NVLString(dr["img"]),
                                mobile = Util.NVLString(dr["mobile"]),
                                name = Util.NVLString(dr["name"]),
                                //price = Util.NVLString(dr["price"]),
                                price = Util.NVLString(Util.NVLInt(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLInt(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                                create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                                bank_name = Util.NVLString(dr["bank_name"]),
                                acc_no = Util.NVLString(dr["acc_no"]),
                                acc_name = Util.NVLString(dr["acc_name"]),
                                fullname = Util.NVLString(dr["username"]),

                                //id = Util.NVLInt(dr["id"]),
                                //token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                                //hp = Util.NVLString(dr["code"]),
                                //img = Util.NVLString(dr["img"]),
                                //name = Util.NVLString(dr["name"]),
                                ////price = Util.NVLString(dr["price"]),
                                //price = Util.NVLString(Util.NVLInt(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLInt(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                                //create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                                //bank_name = Util.NVLString(dr["sale_by"].ToString()!="" ? dr["bank_name_sale"].ToString() : dr["bank_name"].ToString()),
                                //acc_no = Util.NVLString(dr["sale_by"].ToString() != "" ? dr["acc_no_sale"].ToString() : dr["acc_no"].ToString()),
                                //acc_name = Util.NVLString(dr["sale_by"].ToString() != "" ? dr["acc_name_sale"].ToString() : dr["acc_name"].ToString()),
                                //fullname = Util.NVLString(dr["sale_by"].ToString() != "" ? dr["fullname_sale"].ToString() : dr["fullname"].ToString()),

                            });

                        }
                    }
                    /*
                    dt = GetStoredProc("PD063_GET_PRODUCT_SALE", new string[] { "@memberid", "@productid" }, new string[] { Util.NVLString(Varible.User.member_id), Util.NVLString(product_id) });
                    foreach (DataRow dr in dt.Rows)
                    {
                        data.tab2.Add(new Order2
                        {
                            id = Util.NVLInt(dr["id"]),
                            token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                            hp = Util.NVLString(dr["code"]),
                            img = Util.NVLString(dr["img"]),
                            name = Util.NVLString(dr["name"]),
                            //price = Util.NVLString(dr["price"]),
                            price = Util.NVLString(Util.NVLInt(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLInt(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                            create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                            bank_name = Util.NVLString(dr["bank_name"]),
                            acc_no = Util.NVLString(dr["acc_no"]),
                            acc_name = Util.NVLString(dr["acc_name"]),
                            fullname = Util.NVLString(dr["username"]),

                            //id = Util.NVLInt(dr["id"]),
                            //token = Util.NVLString(DataCryptography.Encrypt(dr["id"].ToString())),
                            //hp = Util.NVLString(dr["code"]),
                            //img = Util.NVLString(dr["img"]),
                            //name = Util.NVLString(dr["name"]),
                            ////price = Util.NVLString(dr["price"]),
                            //price = Util.NVLString(Util.NVLInt(dr["price"]) < 1000 ? dr["price"].ToString() : Util.NVLInt(dr["price"]).ToString("0,0", CultureInfo.InvariantCulture)),
                            //create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                            //bank_name = Util.NVLString(dr["sale_by"].ToString()!="" ? dr["bank_name_sale"].ToString() : dr["bank_name"].ToString()),
                            //acc_no = Util.NVLString(dr["sale_by"].ToString() != "" ? dr["acc_no_sale"].ToString() : dr["acc_no"].ToString()),
                            //acc_name = Util.NVLString(dr["sale_by"].ToString() != "" ? dr["acc_name_sale"].ToString() : dr["acc_name"].ToString()),
                            //fullname = Util.NVLString(dr["sale_by"].ToString() != "" ? dr["fullname_sale"].ToString() : dr["fullname"].ToString()),

                        });

                    }
                    */
                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<OrderHis> GetOrderHis()
        {
            try
            {
                OrderDto data = new OrderDto();
                dt = GetStoredProc("PD074_GET_HIS_ORDER", new string[] { "@member_id" }, new string[] { Util.NVLString(Varible.User.member_id) });
                foreach (DataRow dr in dt.Rows)
                {
                    #region stutus
                    /*  1 = รออนุมัติ    승인을 기다립니다
                        2 = อนุมัติ    승인하다
                        3 = ซื้อ   구입
                        4 = ขาย   팔다 */
                    string status_caption = string.Empty;
                    switch (Util.NVLInt(dr["status"]))
                    {
                        case 1:
                            status_caption = "승인을 기다립니다";
                            break;
                        case 2:
                            status_caption = "승인하다";
                            break;
                        case 3:
                            status_caption = "구입";
                            break;
                        case 4:
                            status_caption = "팔다";
                            break;
                        default:
                            break;

                    };
                    #endregion
                    data.orderHis.Add(new OrderHis
                    {
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                        status = status_caption,
                        his_name = Util.NVLString(dr["name"]),
                        amount = Util.NVLString(Util.NVLInt(dr["amount"]).ToString("0,0", CultureInfo.InvariantCulture))
                    }); ;

                }
                return data.orderHis;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SaveData(int orderId, int id, string name, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD064_SAVE_ORDER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(id));
                AddSQLParam(param, "@orderId", Util.NVLInt(orderId));
                AddSQLParam(param, "@member_id", Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@buy_info", Util.NVLString(name));
                AddSQLParam(param, "@action", action);
                conn.Open();
                //cmd.ExecuteNonQuery();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }


            return result;
        }
        public List<MemberListDto> GetUserdata(int orderId)
        {
            try
            {
                List<MemberListDto> list = new List<MemberListDto>();
                dt = GetStoredProc("PD094_GET_USER", new string[] { "@orderId" }, new string[] { Util.NVLString(orderId) });
                foreach (DataRow dr in dt.Rows)
                {

                    list.Add(
                        new MemberListDto
                        {
                            id = Util.NVLInt(dr["id"]),
                            adviser = Util.NVLInt(dr["adviser"]),
                        });
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string SaveInterest(int id, int interest)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD095_SAVE_INTEREST", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLInt(id));
                AddSQLParam(param, "@interest", Util.NVLInt(interest));
                conn.Open();
                //cmd.ExecuteNonQuery();
                MySqlDataReader read = cmd.ExecuteReader();
                while (read.Read())
                {
                    result = read.GetString(0).ToString();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }

            return result;
        }




        public string Cancel(int id)
        {
            string result = "OK";
            //try
            //{
            //    conn = CreateConnection();
            //    MySqlCommand cmd = new MySqlCommand("PD049_BUY_SALE_PRODUCT", conn);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    MySqlParameterCollection param = cmd.Parameters;
            //    param.Clear();
            //    AddSQLParam(param, "@p_id", Util.NVLInt(model.id));
            //    AddSQLParam(param, "@name", Util.NVLString(model.name));
            //    AddSQLParam(param, "@img_head", Util.NVLString(model.img_head));
            //    AddSQLParam(param, "@img_tail", Util.NVLString(model.img_tail));
            //    AddSQLParam(param, "@detail_hot", Util.NVLString(model.detail_hot));
            //    AddSQLParam(param, "@detail_tail", Util.NVLString(model.detail_tail));
            //    AddSQLParam(param, "@detail_hot_mobile", Util.NVLString(model.detail_hot_mobile));
            //    AddSQLParam(param, "@detail_tail_mobile", Util.NVLString(model.detail_tail_mobile));
            //    AddSQLParam(param, "@faq_order", Util.NVLInt(model.faq_order));
            //    AddSQLParam(param, "@member_id", Util.NVLInt(1));
            //    AddSQLParam(param, "@active", Util.NVLInt(model.active));
            //    AddSQLParam(param, "@status", action);

            //    conn.Open();
            //    MySqlDataReader read = cmd.ExecuteReader();
            //    while (read.Read())
            //    {
            //        result = read.GetString(0).ToString();
            //    }
            //    conn.Close();
            //}
            //catch (Exception e)
            //{
            //    result = e.Message.ToString();
            //}
            return result;
        }
        public string WithdrawPoint(string price, int status)
        {
            string result = "OK";
            try
            {
                string name = status == 1 ? ConfigurationManager.AppSettings["withdraw"] : ConfigurationManager.AppSettings["interest"];
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD046_SAVE_WITHDRAWAL", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_name", Util.NVLString(name));
                AddSQLParam(param, "@p_price", Util.NVLString(price));
                AddSQLParam(param, "@user_id", Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@p_status", Util.NVLInt(status));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
            }
            return result;
        }
    }
}

