﻿using AHCBL.Component.Common;
using AHCBL.Dto.User;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dao.User
{
    public class MainDao : BaseDao<MainDao>
    {
        private MySqlConnection conn;
        private DataTable dt;
        public List<MainDto> GetDataList()
        {
            try
            {
                List<MainDto> list = new List<MainDto>();
                dt = GetStoredProc("PD058_GET_PRODUCT_QTY");
                foreach (DataRow dr in dt.Rows)
                {
                    list.Add(
                    new MainDto
                    {
                        id = Util.NVLInt(dr["id"]),
                        amt = Util.NVLString(dr["name"]),
                        price = Util.NVLString(Util.NVLInt(dr["name"]) < 1000 ? dr["name"].ToString() : Util.NVLDecimal(dr["name"]).ToString("0,0", CultureInfo.InvariantCulture)),
                        create_date = Util.NVLString(Cv.Date(Convert.ToDateTime(dr["create_date"]).ToString("yyyyMMdd"))),
                    });
                }

                return list;
            }
            catch (Exception ex)
            {
                //logger.Error(ex);
                throw ex;
            }
        }
        public string SaveDataList(int id, string fee, string amt, string total, string interest, string action)
        {
            string result = "OK";
            try
            {
                conn = CreateConnection();
                MySqlCommand cmd = new MySqlCommand("PD100_SAVE_ORDER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                MySqlParameterCollection param = cmd.Parameters;
                param.Clear();
                AddSQLParam(param, "@p_id", Util.NVLString(id));
                AddSQLParam(param, "@fee", Util.NVLString(fee));
                AddSQLParam(param, "@amt", Util.NVLString(amt));
                AddSQLParam(param, "@total", Util.NVLString(total));
                AddSQLParam(param, "@interest", Util.NVLString(interest));
                AddSQLParam(param, "@user_id", Util.NVLInt(Varible.User.member_id));
                AddSQLParam(param, "@action", Util.NVLString(action));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch(Exception ex)
            {
                throw ex;                
            }
            return result;
        }
    }
}
