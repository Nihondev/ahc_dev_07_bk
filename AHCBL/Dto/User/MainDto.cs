﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.User
{
    public class MainDto
    {
        public int id { get; set; }
        public string price { get; set; }
        public string amt { get; set; }
        public string create_date { get; set; }
    }
}
