﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class CategoryListDto
    {
        public int id { get; set; }
        
        public int num { get; set; }
        [MaxLength(5)]
        public string code { get; set; }

        [MaxLength(50)]
        public string name { get; set; }
        public string min_price { get; set; }
        public string max_price { get; set; }

        [MaxLength(20)]
        public string charging_time { get; set; }
        public int energy { get; set; }
        public int revenue { get; set; }
        public int day { get; set; }
        public int win_count { get; set; }
        public string win_max_price { get; set; }
        public string ca_order { get; set; }
        public bool ca_use { get; set; }
        public int active { get; set; }
        public int product_amt { get; set; }

    }
}
