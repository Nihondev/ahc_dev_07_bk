﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AHCBL.Dto.Admin
{
    public class RequestBuyDto
    {
        public List<RequestBuy> tab1 { get; set; }
        public List<RequestBuy> tab2 { get; set; }

        public RequestBuyDto()
        {
            tab1 = new List<RequestBuy>();
            tab2 = new List<RequestBuy>();
        }
        public class RequestBuy
        {
            public int id { get; set; }
            public int num { get; set; }
            public int product_id { get; set; }
            public string username { get; set; }
            public string fullname { get; set; }
            public string product_name { get; set; }
            public string amount { get; set; }
            public string price { get; set; }
            public string buy_date { get; set; }
            public string process { get; set; }
            public int status { get; set; }
            public int create_by { get; set; }
        }
    }
}
